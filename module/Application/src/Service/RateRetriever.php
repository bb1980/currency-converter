<?php

namespace Application\Service;

use Zend\Http\Request;
use Zend\Http\Client;
use Zend\Stdlib\Parameters;

/*
 * Used for accessing public accessible api to retrieve currency rates.
 */
class RateRetriever
{

    protected $table;

    protected $code;

    public function __construct($table = "A", $code = "RUB")
    {
        $this->table = $table;
        $this->code = $code;
    }

    protected function getRatesApiUri()
    {
        $table = $this->table;
        $code = $this->code;
        $ratesApiUri = "http://api.nbp.pl/api/exchangerates/rates/{$table}/{$code}/";
        return $ratesApiUri;
    }

    protected function getRequestUri()
    {
        $request = new Request();
        $request->getHeaders()->addHeaders(array(
            'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'
        ));
        $request->setUri($this->getRatesApiUri());
        $request->setMethod('GET');
        return $request;
    }

    public function getCurrencyRate($code = "RUB")
    {
        $client = new Client();
        $response = $client->dispatch($this->getRequestUri());
        $data = json_decode($response->getBody(), true);

        $rate = $data['rates'][0]['mid'];
        return $rate;
    }

    public function calculate($amount = 0)
    {
        if(is_numeric($amount)) {
            $currencyRate = $this->getCurrencyRate($this->code);
            $response = $amount * $currencyRate;
        } else {
            $response = "Invalid input data";
        }
        return $response;
    }
}
