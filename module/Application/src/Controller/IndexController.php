<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Application\Service\RateRetriever;

class IndexController extends AbstractActionController
{

    /*
     * Show form to a user.
     */
    public function indexAction()
    {
        return new ViewModel();
    }

    /*
     * Get post data, use service to create response.
     */
    public function postAction()
    {
        if($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
            $rater = new RateRetriever();
            $calculatedValue = $rater->calculate($data['amount']);
            return new JsonModel([
                'response'=>$calculatedValue
            ]);
        }
        return $this->redirect()->toRoute('application', ['action' => 'index']);

    }

}
